import datetime
import jwt
import random

def make_agenda_start_and_end():
    today_Date = datetime.date.today()
    today_Date = str(today_Date)
    agenda_start = today_Date+"T08:00:00"
    agenda_end = today_Date+"T18:00:00"
    agenda = {"agenda_start": agenda_start, "agenda_end": agenda_end}
    return agenda

def update_payload_with_agenda_start_and_end(payload):
    agenda = make_agenda_start_and_end()
    payload["shift_start"] = agenda["agenda_start"]
    payload["shift_end"] = agenda["agenda_end"]
    return payload

def __decoded_jwt(encoded_jwt):
    jwt_options = {
        'verify_signature': False,
        'verify_exp': False,
        'verify_nbf': False,
        'verify_iat': True,
        'verify_aud': False
        }
    decoded = jwt.decode(encoded_jwt, None, algorithms='HS256', options=jwt_options)
    return decoded

def Get_driver_id(encoded_jwt):
    "get the driver id by decoding the jwt provided in arg"
    result = __decoded_jwt(encoded_jwt)
    driver_id = result["driver"]["id"]
    return driver_id

def Get_Random_Device(my_dict):
    random_device = random.choice(list(my_dict))
    return random_device


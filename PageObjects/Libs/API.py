import requests
from Utils import *

def API_Create_Driver_Agenda(env,agent_jwt,payload):
    try:
        url = "https://svc-yuso-web."+env+".yuso.cloud/api/v4/back_office/shifts"
        headers = {
            'Accept': 'application/json',
            'Authorization': "Bearer "+agent_jwt,
            'Yuso-Saas-Company': '745d8eab-9d9e-4cf5-b40f-df8f1ae35f8b',
            'Yuso-Saas-Office': '63439145-83e7-4d31-9d15-0055d1b53cd0'
              }
        print(headers)
        payload=update_payload_with_agenda_start_and_end(payload)
        print(payload)
        response = requests.post(url, headers=headers, json=payload)
        print(response.status_code)
        print(response.content)
        response = response.json()
        print(response)
        return response["id"]
    except:
        response = "Agenda Already Created"
        print(response)
        return response
    
def API_Connect_Agent(env,agent_mail,agent_password,agent_saas_office):
    try:
        url = "https://svc-yuso-web."+env+".yuso.cloud/api/v3/back_end_user/authenticate"
        headers = { 'Accept': 'application/json'}
        payload = {
            "email": agent_mail,
            "password": agent_password,
            "saas_office_name": agent_saas_office
        }
        response = requests.post(url, headers=headers, json=payload)
        print(response.status_code)
        print(response.content)
        response = response.json()
        print(response)
        return response["jwt"]
    except:
        raise Exception("Problem with Agent Authentication")

    

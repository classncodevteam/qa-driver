*** Settings ***
Variables                 ${EXECDIR}/Data/${Device_location}.py
Variables                 ${EXECDIR}/GlobalProperties.py
Library                   ${EXECDIR}/PageObjects/Libs/API.py
Library                   AppiumLibrary
Library                   Collections
*** Variables ***

${remote_url}             https://${Device_provider_URL}/wd/hub
${AllowPopupCap_iOS}      autoAcceptAlerts
${AllowPopupCap_Android}  autoGrantPermissions
&{mobileProperties}       remote_url=${remote_url}                deviceName=${device}
...                       platformName=${platform}                platformVersion=${platformVersion}
...                       language=${language}                    locale=${language}
...                       ${AllowPopupCap_${platform}}=true       phoneOnly=true

*** Keywords ***
Start Driver App
  [Documentation]                         This keyword used to open the driver application using the server url and mobile capabilities
  [Arguments]                             ${Devices}
  ${device} =                             Get Random Device       ${Devices}
  Log                                     ${device}                            
  Run Keyword                             Open app In ${Device_location}       ${device}      
  
Open App In SauceLabs
  [Arguments]                             ${deviceName}
  Log                                     ${mobileProperties}
  Set To Dictionary                       ${mobileProperties}                 deviceName                        ${deviceName}
  Set To Dictionary                       ${mobileProperties}                 platformVersion                   ${Devices_${platform}}[${deviceName}]
  Set To Dictionary                       ${mobileProperties}                 tags                              SauceTest
  Set To Dictionary                       ${mobileProperties}                 testobject_suite_name             Scheduled_run
  Set To Dictionary                       ${mobileProperties}                 appiumVersion                     ${AppiumVersion}
  ${api_key} =                            Set Variable                        ${apikey${platform}}[driver]
  Set To Dictionary                       ${mobileProperties}                 testobject_api_key                ${api_key}
  Log                                     ${mobileProperties}
  Open Application                        &{mobileProperties}

Create and Confirm Diver Agenda
   [Arguments]    ${environment}     ${agent_mail}    ${agent_password}     ${agent_saas_office}  ${payload_create_driver_agenda}
   ${agent_jwt}=    API Connect Agent    ${environment}    ${agent_mail}    ${agent_password}     ${agent_saas_office}
   API Create Driver Agenda    ${environment}    ${agent_jwt}     ${payload_create_driver_agenda} 

   

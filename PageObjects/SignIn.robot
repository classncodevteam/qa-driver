*** Setting ***
Resource                         Common.robot
Variables                        ${EXECDIR}/Locators/${platform}/SignIn.py

*** Variables ***

*** Keywords ***
Sign In The Driver
  [Arguments]    ${driver_mail}    ${driver_password}
  Wait Until Page Contains Element	${SignIn_field_email}    ${wait_period}
  Input Text	${SignIn_field_email}		${driver_mail}
  Hide Keyboard
  Input Text	${SignIn_field_password}		${driver_password}
  Hide Keyboard
  Click Element		${SignIn_btn_signin}
  Wait Until Page Does Not Contain Element    ${SignIn_btn_signin}    
  
  
 

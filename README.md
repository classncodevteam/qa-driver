[![CircleCI](https://circleci.com/bb/classncodevteam/qa-driver.svg?style=shield)](https://circleci.com/bb/classncodevteam/qa-driver)

# QA-Driver Project

This project aims to automate the regression tests for our driver mobile application


### Installation and setup using the QA-installer project

The setup can be done using the QA-installer project which will do all the needed prerequisites like installing Eclipse + RED Plugin, Java, Python3 and it will also clone the project into a local repository. 
 
#1) Clone
 
git clone https://zied_g@bitbucket.org/classncodevteam/qa-installer.git  #with HTTPS (if SSH is not yet configured)

git clone git@bitbucket.org:classncodevteam/qa-installer.git  #with SSH if it is already configured

#2) Setup

change the current directory using cd QA-installer

run the setup script using ./setup.sh 

### Structure of project and features

This project is a keyword-driven and data-driven based on Robot Framework,it uses the Page Object pattern,it contains a set of different test suites like for example the Sign in and sign up suites.


### Execution
robot -T -d Reports/ <name_of_the_test_file.robot>

-T - Short for --timestampoutputs. Creates reports, logs, etc. with the current timestamp so we don't overwrite existing ones upon execution.

-d - Short for --outputdir. Tells the framework where to create the report files.

-i - Short for --include which means "include for testing only those test cases with this tag

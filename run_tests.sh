#!/bin/bash

which_platform() {
	i=0
	PS3='Your choice: '
	options=("$2" "$3" exit)
	select opt in "${options[@]}"
	do
		i=$(($i + 1))
		if [ $i -eq  3 ] ; then return -1 && exit  ; fi
		 
		case "$REPLY" in
			1|"$2")
				local choix="$2"
				break
			    	;;
			2|"$3")
				local choix="$3"
				break
			    	;;
			*)
			echo "invalid option $REPLY" >&2
		   	;;
		    esac
	done
	echo $choix
}

platform=$(which_platform platform Android iOS)
echo "---"
saascmp=$(which_platform saascmp marcel bod)
echo "---"
cloud=$(which_platform cloud SauceLabs BrowserStack)
echo "---"
read -p "Which test tag [ wip SignUp SignIn SideMenu Driver ]: " tag

robot -x xUnit -T -d Reports/$platform-$saascmp -v RunByCI:False -v platform:$platform  -v platformVersion:13 -v device:"iPhone XS" -v saascmp:$saascmp -v Device_location:$cloud -i $tag Tests/


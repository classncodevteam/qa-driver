*** Settings ***
Documentation                          Scenarios for Driver Signin feature
Resource                               ${EXECDIR}/PageObjects/Common.robot
Resource                               ${EXECDIR}/PageObjects/SignIn.robot
Variables                              Test_Data.py

*** Test Cases ***

Sign IN OK
  [Tags]    Driver    SignIn     wip
  [Setup]   Create and Confirm Diver Agenda  ${environment}    ${agent_mail}  ${agent_password}
  ...  ${agent_saas_office}  ${payload_driver_agenda}
  Start Driver App      ${Devices_${platform}}
  Sign In The Driver    ${driver_mail}    ${driver_password}

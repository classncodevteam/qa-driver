#Device_location: which Device provider to use: "local", "SauceLabs", "BrowserStack"
Device_location = 'SauceLabs'

#Misc
wait_period = '10 seconds'
DELAY = '0.2 seconds'
environment= 'rel'
platform = 'Android'
platformVersion = '10.0'
device = 'Samsung_Galaxy_A5_2017_real'
saascmp = 'marcel'
language = 'EN'

# Appium configuration
AppiumVersion = '1.16.0'